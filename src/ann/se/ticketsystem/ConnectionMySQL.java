package ann.se.ticketsystem;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Formatter;

//import objects.Airport;

/**
 * Databaskoppling mot MySQL
 * @author Ann
 *
 */
public class ConnectionMySQL {

	public static String url = "jdbc:mysql://localhost/";
	public static String user = "root";
	public static String password = "";
	public static String database = "ticketSystem";

	// public static String table ="airport";
	// private static Statement myStmt=null;
	// "Source mydb.sql";
	public ConnectionMySQL() {

	}

	/**
	 * Koppling mot MySQL
	 * @return
	 */
	public static Connection dbConnector() {
		Connection myConn = null;
		Statement myStmt = null;
		try {
			Class.forName("com.mysql.jdbc.Driver");
			// 1. Get a connection to database
			myConn = DriverManager.getConnection(url, user, password);

			// 2. Create a statement
			myStmt = myConn.createStatement();

		} catch (ClassNotFoundException | SQLException se) {
			se.printStackTrace();
		} finally {
			try {
				if (!(myConn == null)) {
					myConn.close();
					myConn = null;
				}
			} catch (SQLException se) {
			}
			try {
				if (!(myStmt == null)) {
					myStmt.close();
					myStmt = null;
				}
			} catch (SQLException se) {
			}

		}
		return myConn;
	}

	/**
	 * Raderar databasen 
	 */
	public static void deleteMysql() {
		Connection myConn = null;
		Statement myStmt = null;
		try {
			Class.forName("com.mysql.jdbc.Driver");
			myConn = DriverManager.getConnection(url, user, password);
			myStmt = myConn.createStatement();

			// ----------Query skapa databas om den inte existerar
			String query = "DROP SCHEMA " + database;

			myStmt.executeUpdate(query);
		} catch (ClassNotFoundException | SQLException se) {

		} finally {
			try {
				if (!(myConn == null)) {
					myConn.close();
					myConn = null;
				}
			} catch (SQLException se) {
			}
			try {
				if (!(myStmt == null)) {
					myStmt.close();
					myStmt = null;
				}
			} catch (SQLException se) {
			}
		}
	}

	/**
	 * Skapar en tom databas och tabeller
	 */
	public static void createMysql() {
		Connection myConn = null;
		Statement myStmt = null;
		try {
			Class.forName("com.mysql.jdbc.Driver");
			myConn = DriverManager.getConnection(url, user, password);
			myStmt = myConn.createStatement();

			// ----------Query skapa databas om den inte existerar
			String query = "CREATE SCHEMA IF NOT EXISTS " + database;

			// ----------Query1-8 skapar tabeller om de inte existerar
			// ----------Query1 tabell Airport
			String query1 = "CREATE TABLE IF NOT EXISTS " + database
					+ ".airport"
					+ "(airport_id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,"
					+ "prefix VARCHAR(3) UNIQUE," + "city VARCHAR(40),"
					+ "country VARCHAR(40)," + "create_date TIMESTAMP)";
//			System.out.println(query1);

			// ----------Query2 tabell Passenger
			String query2 = "CREATE TABLE IF NOT EXISTS " + database
					+ ".passenger"
					+ "(passenger_id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,"
					+ "firstName VARCHAR(40)," + "lastName VARCHAR(40),"
					+ "email VARCHAR(40)," + "phoneNumber VARCHAR(40),"
					+ "create_date TIMESTAMP)";
//			System.out.println(query2);

			// ----------Query3 tabell Company
			String query3 = "CREATE TABLE IF NOT EXISTS " + database
					+ ".company"
					+ "(company_id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,"
					+ "prefix VARCHAR(3)," + "name VARCHAR(40),"
					+ "employees INT," + "airplanes INT,"
					+ "create_date TIMESTAMP)";
//			System.out.println(query3);

			// ----------Query4 tabell Flightline
			String query4 = "CREATE TABLE IF NOT EXISTS "
					+ database
					+ ".flightline"
					+ "(flightline_id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,"
					+ "name VARCHAR(40),"
					+ "arrAirport VARCHAR(40),"
					+ "depAirport VARCHAR(40),"
					+ "arr_date VARCHAR(40),"
					+ "company_id INT,"
					+ "airport_id INT,"
					+ "FOREIGN KEY (company_id) REFERENCES company(company_id),"
					+ "create_date TIMESTAMP" + ")";
//			System.out.println(query4);


			// ----------Query5 tabell Ticket
			String query5 = "CREATE TABLE IF NOT EXISTS "
					+ database
					+ ".ticket"
					+ "(ticket_id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,"
					+ "passenger_id INT,"
					+ "flightline_id INT,"
					+ "bookingNumber VARCHAR(40),"
					+ "FOREIGN KEY (passenger_id) REFERENCES passenger(passenger_id),"
					+ "FOREIGN KEY (flightline_id) REFERENCES flightline(flightline_id),"
					+ "create_date TIMESTAMP" + ")";
//			System.out.println(query5);

			myStmt.executeUpdate(query);
			myStmt.executeUpdate(query1);
			myStmt.executeUpdate(query2);
			myStmt.executeUpdate(query3);
			myStmt.executeUpdate(query4);
			myStmt.executeUpdate(query5);

		} catch (ClassNotFoundException | SQLException se) {
			se.printStackTrace();
		}
	}

	/**
	 * Lägger in defaultvärden i alla tabeller
	 */
	static void insertValues() {

		Connection myConn = null;
		Statement myStmt = null;
		try {
			Class.forName("com.mysql.jdbc.Driver");
			myConn = DriverManager.getConnection(url, user, password);
			myStmt = myConn.createStatement();

			// ----------Query1-8 skapar värden till tabellerna
			// ----------Query1 tabell Airport

			String query1 = "INSERT INTO " + database
					+ ".airport (prefix, city, country, create_date) "
					+ "VALUES('SKE', 'Skellefteå', 'Sverige',now()),"
					+ "('KRN', 'Kiruna', 'Sverige',now()),"
					+ "('ARN', 'Stockholm Arlanda', 'Sverige',now()),"
					+ "('BMA', 'Stockholm Bromma', 'Sverige',now()),"
					+ "('RNY', 'Ronneby', 'Sverige',now()),"
					+ "('LUL', 'Luleå', 'Sverige',now()),"
					+ "('GOT', 'Göteborg', 'Sverige',now()),"
					+ "('SND', 'Sundsvall', 'Sverige',now()),"
					+ "('OSD', 'Östersund', 'Sverige',now()),"
					+ "('UME', 'Umeå', 'Sverige',now()),"
					+ "('MMO', 'Malmö', 'Sverige',now()),"
					+ "('TMS', 'Trondheim', 'Norge',now()),"
					+ "('FRK', 'Fredriksholm', 'Sverige',now()),"
					+ "('DSO', 'Delsbo', 'Sverige',now()),"
					+ "('ABB', 'Abbekås', 'Sverige',now()),"
					+ "('NRK', 'Norrköping', 'Sverige',now())";


//			System.out.println(query1);

			// ----------Query2 tabell Passenger
			String query2 = "INSERT INTO "
					+ database
					+ ".passenger (firstname, lastname, email, phoneNumber,create_date) "
					+ "VALUES('Aaa', 'Aaaaa', 'aaa.aaaa@aa.aa','0000000001',now()),"
					+ "('Mathias', 'Björk', 'mathias.bjork@vattenreglering.se','0702222222',now()),"
					+ "('Mari', 'Jansson', 'marij14@hotmail.com','0703333333',now()),"
					+ "('Linda', 'Håkansson', 'lindahakan@hotmail.com','07055108777',now()),"
					+ "('Ann', 'Johansson', 'ann@hotmail.com','0702696401',now()),"
					+ "('Anna', 'Andersson', 'anna.andersson@hotmail.com','07012345678',now()),"
					+ "('Lennart', 'Nilsson', 'lelle@hotmail.com','0702249087',now()),"
					+ "('Jocke', 'Svensson', 'jocken@hotmail.com','0709876543',now()),"
					+ "('Kia', 'Nilsson', 'kias@hotmail.com','005858588',now()),"
					+ "('Lars', 'Nordström', 'larsnordstrom@me.com','0705888236',now()),"
					+ "('Kicki', 'Lindqvist', 'linkan@hotmail.com','07045678912',now())";					

//			System.out.println(query2);

			// ----------Query3 tabell Company
			String query3 = "INSERT INTO "
					+ database
					+ ".company (prefix, name, employees, airplanes,create_date) "
					+ "VALUES('BR','Bromma_Airlines', 25, 4,now()),"
					+ "('SK','Scandinavian_Airlines', 327, 93,now()),"
					+ "('PR','Philippine_Air', 56, 18,now()),"
					+ "('MO','Malmö_Aviation', 98, 32,now()),"
					+ "('JF','Jamtflyg', 9, 2,now()),"
					+ "('RU','Russian_Airlines', 43, 21,now()),"
					+ "('SA','SkövdeAir', 15, 3,now()),"
					+ "('AR','Åre_Flyg', 12, 1,now())";
//			System.out.println(query3);

			// ----------Query4 tabell Flightline
			String query4 = "INSERT INTO "
					+ database
					+ ".flightline (name, arrAirport, depAirport, arr_date, company_id, create_date) "
					+ "VALUES('AA0001', 'AAA', 'AAA','19000101',1,now()),"
					+ "('BM1435', 'RNY', 'ARN','20150220',3,now()),"
					+ "('BM1536', 'RNY', 'MMO','20150224',3,now()),"
					+ "('SK1489', 'OSD', 'BMA','20150128',2,now()),"
					+ "('SK1489', 'OSD', 'BMA','20150129',2,now()),"
					+ "('SA4040', 'NRK', 'FRK','20150109',7,now()),"
					+ "('SA4040', 'NRK', 'FRK','20150125',7,now()),"
					+ "('SA4040', 'NRK', 'FRK','20150130',7,now()),"
					+ "('AR1231', 'OSD', 'ARN','20150205',8,now()),"
					+ "('AR1231', 'OSD', 'ARN','20150206',8,now()),"
					+ "('AR1231', 'OSD', 'ARN','20150207',8,now())";

//			System.out.println(query4);


			// ----------Query5 tabell Ticket
			String query5 = "INSERT INTO "
					+ database
					+ ".ticket (passenger_id, flightline_id, bookingnumber,create_date) VALUES(3, 2, '0703333333',now()),"
					+ "(2, 1,'0702222222',now()),"
					+ "(5, 8,'0702696401',now())";


//			System.out.println(query5);

			myStmt.executeUpdate(query1);
			myStmt.executeUpdate(query2);
			myStmt.executeUpdate(query3);
			myStmt.executeUpdate(query4);
			myStmt.executeUpdate(query5);

		} catch (ClassNotFoundException | SQLException se) {
			se.printStackTrace();
		}
	}

	/**
	 * Uppdaterar eller lägger till uppgifter i tabeller baserat på MySQL fråga
	 * @param query
	 */
	public static void updateOrInsert(String query) {
		Connection myConn = null;
		Statement myStmt = null;

		try {
			Class.forName("com.mysql.jdbc.Driver");
			// 1. Get a connection to database
			myConn = DriverManager.getConnection(url, user, password);

			// 2. Create a statement
			myStmt = myConn.createStatement();

			myStmt.executeUpdate(query);

		} catch (ClassNotFoundException | SQLException se) {

		} finally {
			try {
				if (!(myConn == null)) {
					myConn.close();
					myConn = null;
				}
			} catch (SQLException se) {
			}
			try {
				if (!(myStmt == null)) {
					myStmt.close();
					myStmt = null;
				}
			} catch (SQLException se) {
			}
		}

	}

	/**
	 * Kontroller om något är unikt
	 * @param s
	 * @return
	 */
	public static boolean checkIfUnique(String s) {
		boolean isUnique = true;

		Connection myConn = null;
		Statement myStmt = null;
		ResultSet rs = null;

		try {
			// Register the class.
			Class.forName("com.mysql.jdbc.Driver");
			// Create a connection.
			myConn = DriverManager.getConnection(url, user, password);
			// Create a statement.
			myStmt = myConn.createStatement();
			rs = myStmt.executeQuery(s);

			while (rs.next()) {
				isUnique = false;
			}
		} catch (ClassNotFoundException | SQLException se) {

		} finally {
			try {
				if (!(myConn == null)) {
					myConn.close();
				}
			} catch (SQLException se) {
			}
			try {
				if (!(myStmt == null)) {
					myStmt.close();
				}
			} catch (SQLException se) {

			}
		}
		return isUnique;
	}

	/** Hämtar uppgifter från klassen Company
	 * @param query
	 * @return
	 */
	public static Airport getAirportInstance(String query) {

		Connection myConn = null;
		Statement myStmt = null;
		ResultSet rs = null;
		String create_date = null;
		String city = null, prefix = null, country = null;
		int airport_id = 0;

		try {
			Class.forName("com.mysql.jdbc.Driver");
			myConn = DriverManager.getConnection(url, user, password);
			myStmt = myConn.createStatement();

			rs = myStmt.executeQuery(query);

			while (rs.next()) {
				airport_id = rs.getInt("airport_id");
				prefix = rs.getString("prefix");
				city = rs.getString("city");
				country = rs.getString("country");
				create_date = rs.getString("create_date");

			}

		} catch (ClassNotFoundException | SQLException se) {

		} finally {
			try {
				if (!(myConn == null)) {
					myConn.close();
					myConn = null;
				}
			} catch (SQLException se) {
			}
			try {
				if (!(myStmt == null)) {
					myStmt.close();
					myStmt = null;
				}
			} catch (SQLException se) {
			}
		}
		return new Airport(airport_id, prefix, city, country, create_date);

	}

	/** Hämtar uppgifter från klassen Airport
	 * @param query
	 * @return
	 */
	public static Company getCompanyInstance(String query) {
		Connection myConn = null;
		Statement myStmt = null;
		ResultSet rs = null;
		String create_date = null;
		String name = null, prefix = null;
		int employees = 0;
		int airplanes = 0;
		int company_id = 0;

		try {
			Class.forName("com.mysql.jdbc.Driver");
			myConn = DriverManager.getConnection(url, user, password);
			myStmt = myConn.createStatement();

			rs = myStmt.executeQuery(query);

			while (rs.next()) {
				company_id = rs.getInt("company_id");
				prefix = rs.getString("prefix");
				name = rs.getString("name");
				employees = rs.getInt("employees");
				airplanes = rs.getInt("airplanes");
				create_date = rs.getString("create_date");

			}

		} catch (ClassNotFoundException | SQLException se) {

		} finally {
			try {
				if (!(myConn == null)) {
					myConn.close();
					myConn = null;
				}
			} catch (SQLException se) {
			}
			try {
				if (!(myStmt == null)) {
					myStmt.close();
					myStmt = null;
				}
			} catch (SQLException se) {
			}
		}
		return new Company(company_id, name, employees, airplanes, prefix,
				create_date);

	}

	/**
	 * Hämtar uppgifter från klassen Flightline
	 * @param query
	 * @return
	 */
	public static Flightline getFlightlineInstance(String query) {

		Connection myConn = null;
		Statement myStmt = null;
		ResultSet rs = null;
		String create_date = null;
		String name = null, arrAirport = null, depAirport = null, arr_date = null;
		int flightline_id = 0;

		try {
			Class.forName("com.mysql.jdbc.Driver");
			myConn = DriverManager.getConnection(url, user, password);
			myStmt = myConn.createStatement();

			rs = myStmt.executeQuery(query);

			while (rs.next()) {
				flightline_id = rs.getInt("flightline_id");
				arr_date = rs.getString("arr_date");
				arrAirport = rs.getString("arrAirport");
				depAirport = rs.getString("depAirport");
				name = rs.getString("name");
				create_date = rs.getString("create_date");

			}

		} catch (ClassNotFoundException | SQLException se) {

		} finally {
			try {
				if (!(myConn == null)) {
					myConn.close();
					myConn = null;
				}
			} catch (SQLException se) {
			}
			try {
				if (!(myStmt == null)) {
					myStmt.close();
					myStmt = null;
				}
			} catch (SQLException se) {
			}
		}
		return new Flightline(flightline_id, arr_date, name, arrAirport,
				depAirport, create_date);

	}

	/**
	 * Hämtar uppgifter från klassen Passenger
	 * @param query
	 * @return
	 */
	public static Passenger getPassengerInstance(String query) {

		Connection myConn = null;
		Statement myStmt = null;
		ResultSet rs = null;
		String create_date = null;
		String firstname = null, lastname = null, email = null, phonenumber = null;
		int passenger_id = 0;

		try {
			Class.forName("com.mysql.jdbc.Driver");
			myConn = DriverManager.getConnection(url, user, password);
			myStmt = myConn.createStatement();

			rs = myStmt.executeQuery(query);

			while (rs.next()) {
				passenger_id = rs.getInt("passenger_id");
				firstname = rs.getString("firstname");
				lastname = rs.getString("lastname");
				email = rs.getString("email");
				phonenumber = rs.getString("phonenumber");
				create_date = rs.getString("create_date");

			}

		} catch (ClassNotFoundException | SQLException se) {

		} finally {
			try {
				if (!(myConn == null)) {
					myConn.close();
					myConn = null;
				}
			} catch (SQLException se) {
			}
			try {
				if (!(myStmt == null)) {
					myStmt.close();
					myStmt = null;
				}
			} catch (SQLException se) {
			}
		}
		return new Passenger(passenger_id, firstname, lastname, email,
				phonenumber, create_date);

	}

	/**
	 * Hämtar uppgifter från klassen Ticket
	 * @param query
	 * @return
	 */
	public static Ticket getTicketInstance(String query) {


		Ticket ticket = new Ticket();

		Connection myConn = null;
		Statement myStmt = null;
		ResultSet rs = null;
		int ticket_id = 0;
		int flightline_id = 0;
		String arr_date = null;
		String arrAirport = null;
		String depAirport = null;
		String flightline_name = null;
		String company_name = null;
		String create_date = null;
		int passenger_id = 0;
		String firstName = null;
		String lastName = null;
		String bookingnumber = null;



		try {
			Class.forName("com.mysql.jdbc.Driver");
			myConn = DriverManager.getConnection(url, user, password);
			myStmt = myConn.createStatement();

			rs = myStmt.executeQuery(query);
			rs.beforeFirst();
			while (rs.next()) {
				ticket_id = rs.getInt("Id");				
				arr_date = rs.getString("ArrivalDate");
				arrAirport = rs.getString("ArrAirport");
				depAirport = rs.getString("DepAirport");
				flightline_name = rs.getString("FlightlineName");
				company_name = rs.getString("Company");
				bookingnumber = rs.getString("BookingNumber");
				firstName = rs.getString("FirstName");
				lastName = rs.getString("LastName");
				create_date = rs.getString("CreateDate");

			}

		} catch (ClassNotFoundException | SQLException se) {

		} finally {
			try {
				if (!(myConn == null)) {
					myConn.close();
					myConn = null;
				}
			} catch (SQLException se) {
			}
			try {
				if (!(myStmt == null)) {
					myStmt.close();
					myStmt = null;
				}
			} catch (SQLException se) {
			}
		}
		return ticket;
	}

	public static void getAllTickets(String query) {

		Ticket ticket = new Ticket();

		Connection myConn = null;
		Statement myStmt = null;
		ResultSet rs = null;
		ResultSetMetaData meta = null;

		int ticket_id = 0;
		int flightline_id = 0;
		String arr_date = null;
		String arrAirport = null;
		String depAirport = null;
		String flightline_name = null;
		String company_name = null;
		String create_date = null;
		int passenger_id = 0;
		String firstName = null;
		String lastName = null;
		String bookingnumber = null;



		try {
			Class.forName("com.mysql.jdbc.Driver");
			myConn = DriverManager.getConnection(url, user, password);
			myStmt = myConn.createStatement();

			rs = myStmt.executeQuery(query);
			meta = rs.getMetaData();

			// Array for column names as alias.
			String [] colNames = new String[meta.getColumnCount()];
			//  loop the columns.
			for (int i = 1; i <= meta.getColumnCount(); i++ ){
				// insert the column names to an array.
				//					colNames[i-1] = meta.getColumnName(i);  //ColumnName
				colNames[i-1] = meta.getColumnLabel(i);		//Alias	
				//System.out.print(" " + colNames[i-1] + "\t" );

			}
			System.out
			.println("-------------------------------------------------------------------------------------------------------------------------------------");

			for(String col : colNames){
				System.out.print(col + "\t");
			}
			System.out
			.println("\n-------------------------------------------------------------------------------------------------------------------------------------");


			while (rs.next()) {
				//				ticket.setMyTicket_id(rs.getInt("Id"));		
				ticket_id = rs.getInt("Id");				
				arr_date = rs.getString("Datum");
				arrAirport = rs.getString("Från");
				depAirport = rs.getString("Till");
				flightline_name = rs.getString("Flygnummer");
				company_name = rs.getString("Flygbolag");
				bookingnumber = rs.getString("Bokningsnummer");
				firstName = rs.getString("Förnamn");
				lastName = rs.getString("Efternamn");
				create_date = rs.getString("Skapat datum");
				//System.out.println(ticket.toString());

				Formatter f = new Formatter();
				System.out.println(new Formatter().format("%-5s %-9s %-7s %-7s %-10s %-22s %-13s %-7s %-15s %-10s", ticket_id, arr_date, arrAirport,depAirport,flightline_name, company_name,bookingnumber,firstName,lastName, create_date));
				//				System.out.println(" " + ticket_id + "\t|" + arr_date+ "\t | " +arrAirport+ "\t | " + depAirport+ "\t | " +flightline_name+ "\t | " +company_name+ "\t | " + bookingnumber+ "\t | " +firstName+ "\t | " +lastName+ "\t | " + create_date);
			}
			System.out
			.println("-------------------------------------------------------------------------------------------------------------------------------------");

		} catch (ClassNotFoundException | SQLException se) {

		} finally {
			try {
				if (!(myConn == null)) {
					myConn.close();
					myConn = null;
				}
			} catch (SQLException se) {
			}
			try {
				if (!(myStmt == null)) {
					myStmt.close();
					myStmt = null;
				}
			} catch (SQLException se) {
			}
		}
		//		return new Ticket (ticket_id, arr_date, arrAirport, depAirport, flightline_name, company_name,firstName, lastName, bookingnumber, create_date);
	}


	/**
	 * Visar uppgifter från tabellen Flightline
	 * @param query
	 */
	public static void getFlightline(String query) {

		Connection myConn = null;
		Statement myStmt = null;
		ResultSet rs = null;
//		ResultSetMetaData meta = null;

		int flightline_id = 0;	
		String arr_date = null;
		String arrAirport = null;
		String depAirport = null;
		String name = null;
		String company_name = null;
		String create_date = null;
		// String name, arrAirport, depAirport, nameCompany;

		try {
			Class.forName("com.mysql.jdbc.Driver");
			myConn = DriverManager.getConnection(url, user, password);
			myStmt = myConn.createStatement();

			rs = myStmt.executeQuery(query);


			//			// Array for column names as alias.
			//						String [] colNames = new String[meta.getColumnCount()];
			//							//  loop the columns.
			//							for (int i = 1; i <= meta.getColumnCount(); i++ ){
			//								// insert the column names to an array.
			////								colNames[i-1] = meta.getColumnName(i);  //ColumnName
			//								colNames[i-1] = meta.getColumnLabel(i);		//Alias	
			//								//System.out.print(" " + colNames[i-1] + "\t" );
			//								
			//							}
			//							for(String col : colNames){
			//								System.out.print(col + "\t");
			//							}
			//							System.out.println();
			//			


			if (rs.next()) {
				Formatter f = new Formatter();
				System.out
				.println("------------------------------------------------------------------------------------------");
				System.out.print(f.format("%-5s %-10s %-10s %-5s %-5s %-25s %-20s", "Id",  "Datum", "Flyglinje","Från","Till","Flygbolag","Ändringsdatum"));
				System.out
				.print("\n------------------------------------------------------------------------------------------");
			}
			rs.beforeFirst();
			while (rs.next()) {
				flightline_id = rs.getInt("Id");
				arr_date = rs.getString("Datum");
				name = rs.getString("Flyglinje");
				arrAirport= rs.getString("Från");
				depAirport = rs.getString("Till");
				company_name = rs.getString("Flygbolag");
				create_date = rs.getString("Ändringsdatum");

				System.out.println();
				Formatter f = new Formatter();
				System.out.print(f.format("%-5s %-10s %-10s %-5s %-5s %-25s %-20s", flightline_id, arr_date, name, arrAirport,depAirport,company_name, create_date));
				// System.out.println( prefix+ city+ country+ create_date);

			}
			System.out
			.println("\n------------------------------------------------------------------------------------------\n");
		}  catch (ClassNotFoundException | SQLException se) {
			se.printStackTrace();
		}

	}

	/**
	 * Hämtar uppgifter från tabellen Airport
	 * @param query
	 * @return
	 */
	public static void getAirport(String query) {
		Airport airport = new Airport();
		Connection myConn = null;
		Statement myStmt = null;
		ResultSet rs = null;
		int airport_id = 0;
		String city = null, prefix = null, country = null;
		Date create_date;

		try {
			Class.forName("com.mysql.jdbc.Driver");
			myConn = DriverManager.getConnection(url, user, password);
			myStmt = myConn.createStatement();

			rs = myStmt.executeQuery(query);

			if (rs.next()) {
				Formatter r = new Formatter();
				System.out
				.println("-------------------------------------------------------------------");
				System.out.println(r.format("%-5s %-7s %-20s %-15s %-15s", "Id","Prefix",
						"Stad", "Land", "Ändringsdatum"));
				System.out
				.println("-------------------------------------------------------------------");
			}
			
			rs.beforeFirst();
			while (rs.next()) {
				airport_id = rs.getInt("airport_id");
				prefix = rs.getString("prefix");
				city = rs.getString("city");
				country = rs.getString("country");
				create_date = rs.getDate("create_date");

				airport.setMyPrefix(rs.getString("prefix"));

				Formatter f = new Formatter();
				System.out.println(f.format("%-5s %-7s %-20s %-15s %-15s", airport_id, prefix,
						city, country, create_date));
				// System.out.println( prefix+ city+ country+ create_date);

			}
			System.out
			.println("-------------------------------------------------------------------");

		}  catch (ClassNotFoundException | SQLException se) {
			se.printStackTrace();

		}
	}

	/**
	 * Visar uppgifter från tabellen Company
	 * @param query
	 */
	public static void getCompany(String query) {
		Connection myConn = null;
		Statement myStmt = null;
		ResultSet rs = null;

		String prefix, name;
		int company_id, employees, airplanes;
		Date create_date;

		try {
			Class.forName("com.mysql.jdbc.Driver");
			myConn = DriverManager.getConnection(url, user, password);
			myStmt = myConn.createStatement();

			rs = myStmt.executeQuery(query);
			Formatter r = new Formatter();
			System.out
			.println("------------------------------------------------------------------------------------------");
			System.out.println(r.format("%-5s %-7s %-25s %-10s %-10s %-15s",
					"Id", "Prefix", "Bolag", "Anställda", "Flygplan",
					"Ändringsdatum"));

			System.out
			.println("------------------------------------------------------------------------------------------");

			while (rs.next()) {
				company_id = rs.getInt("company_id");
				prefix = rs.getString("prefix");
				name = rs.getString("name");
				employees = rs.getInt("employees");
				airplanes = rs.getInt("airplanes");
				create_date = rs.getDate("create_date");

				Formatter f = new Formatter();
				System.out.println(f.format(
						"%-5s %-7s %-25s %-10s %-10s %-15s", company_id,
						prefix, name, employees, airplanes, create_date));

			}
			System.out
			.println("------------------------------------------------------------------------------------------");

		} catch (ClassNotFoundException | SQLException se) {
			se.printStackTrace();
		}
	}




}



