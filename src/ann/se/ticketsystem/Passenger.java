package ann.se.ticketsystem;

public class Passenger {
	String myFirstname;
	String myLastname;
	String myEmail;
	String myPhonenumber;
	String create_date;
	int myPassenger_id;


	public Passenger (String firstname, String lastname,
			String email, String phonenumber, String create_date){
		this.myFirstname = firstname;
		this.myLastname = lastname;
		this.myEmail = email;
		this.myPhonenumber = phonenumber;
		this.create_date = create_date;
		this.myPassenger_id = 0;
	}


	public Passenger() {
		// TODO Auto-generated constructor stub
	}



	public Passenger(int passenger_id, String firstname, String lastname,
			String email, String phonenumber, String create_date) {
		this.myFirstname = firstname;
		this.myLastname = lastname;
		this.myEmail = email;
		this.myPhonenumber = phonenumber;
		this.create_date = create_date;
		this.myPassenger_id = passenger_id;
	}


	public String getMyFirstname() {
		return myFirstname;
	}


	public void setMyFirstname(String myFirstname) {
		this.myFirstname = myFirstname;
	}


	public String getMyLastname() {
		return myLastname;
	}


	public void setMyLastname(String myLastname) {
		this.myLastname = myLastname;
	}


	public String getMyEmail() {
		return myEmail;
	}


	public void setMyEmail(String myEmail) {
		this.myEmail = myEmail;
	}


	public String getMyPhonenumber() {
		return myPhonenumber;
	}


	public void setMyPhonenumber(String myPhonenumber) {
		this.myPhonenumber = myPhonenumber;
	}


	public String getCreate_date() {
		return create_date;
	}


	public void setCreate_date(String create_date) {
		this.create_date = create_date;
	}


	public int getMyPassenger_id() {
		return myPassenger_id;
	}


	public void setMyPassenger_id(int myPassenger_id) {
		this.myPassenger_id = myPassenger_id;
	}

	//	public String toString(){
	//		return "" + this.getMyPassenger_id() + ", " + this.getMyFirstname() + ", " + this.getMyLastname() + ", " + this.getMyEmail() + ", " + this.getMyPhonenumber() + "";
	//	}
	//	
	//	public String insertString(){
	//		return  "'" + this.getMyFirstname() + "', '" + this.getMyLastname() + "', '" + this.getMyEmail() + "', '" + this.getMyPhonenumber() + "', NOW()";
	//	}





}
