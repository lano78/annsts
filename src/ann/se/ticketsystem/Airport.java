package ann.se.ticketsystem;



public class Airport {
	int myAirport_id;
	String myPrefix;
	String myCity;
	String myCountry;
	String create_date;



	public Airport(int airport_id, String prefix, String city, String country) {
		this.myAirport_id = airport_id;
		this.myPrefix = prefix;
		this.myCity = city;
		this.myCountry = country;
		this.create_date = create_date;
	}

	public Airport(int airport_id, String prefix, String city, String country,
			String create_date2) {
		// TODO Auto-generated constructor stub
		this.myAirport_id =0;
		this.myPrefix = prefix;
		this.myCity = city;
		this.myCountry = country;
		this.create_date = create_date;
	}

	public Airport() {
		// TODO Auto-generated constructor stub
	}

	public String getMyPrefix() {
		return myPrefix;
	}

	public void setMyPrefix(String myPrefix) {
		this.myPrefix = myPrefix;
	}

	public String getMyCity() {
		return myCity;
	}

	public void setMyCity(String myCity) {
		this.myCity = myCity;
	}

	public String getMyCountry() {
		return myCountry;
	}

	public void setMyCountry(String myCountry) {
		this.myCountry = myCountry;
	}

	public int getMyAirport_id() {
		return myAirport_id;
	}

	public void setMyAirport_id(int myAirport_id) {
		this.myAirport_id = myAirport_id;
	}

}
