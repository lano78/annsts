package ann.se.ticketsystem;

import java.util.ArrayList;
import java.util.Formatter;
import java.util.Scanner;
import java.util.regex.Pattern;

//import objects.Airport;

/**
 * @author Ann
 *
 */
public class Admin {

	public Admin() {

	}

	/**
	 * Validerar flygplats prefix, tre bokstäver
	 * @param s
	 * @return
	 */
	public static boolean validateAirportPrefix(String s) {
		String regex = "^[a-zA-Z]{3}";
		Pattern pattern = Pattern.compile(regex);
		return pattern.matcher(s).matches();
	}

	/**
	 * Skapar ny flygplats
	 * @param user_input
	 */
	public void createNewAirport(Scanner user_input) {

		Airport airport = new Airport();

		String prefix, city, country;
		boolean isUnique = false;

		System.out.println("\nDessa flygplatser finns i systemet:\t");
		ConnectionMySQL.getAirport(Queries.SHOW_AIRPORTS);

		System.out.println("\nSkapa en ny flygplats, skriv in följande:\t");
		System.out.print("\nStad: ");
		city = user_input.next();
		airport.setMyCity(city);

		System.out.print("Land: ");
		country = user_input.next();
		airport.setMyCountry(country);
		do {
			System.out.print("Prefix, tre bokstäver (A-Z): ");
			prefix = user_input.next().toUpperCase();
			isUnique = ConnectionMySQL
					.checkIfUnique(Queries.getAirportWherePrefix(prefix));

			if (!validateAirportPrefix(prefix)) {
				System.out
				.println("\nSkriv in prefix korrekt, ange tre bokstäver (A-Z): ");
			}

			if (!isUnique) {
				System.out
				.println("En flygplats med detta namn finns redan, ange ett nytt: ");
			}

		} while (((!isUnique) || !validateAirportPrefix(prefix)));

		airport.setMyPrefix(prefix);

		System.out.println("\nNu finns (" + airport.getMyPrefix() + " "
				+ airport.getMyCity() + ") inlagd i databas\n");

		ConnectionMySQL.updateOrInsert(Queries.insertAirport(airport));
		ConnectionMySQL.getAirport(Queries.SHOW_AIRPORTS);

	}

	//	/**
	//	 * Hämtar alla flygplatser
	//	 * @param user_input
	//	 * @return
	//	 */
	//	public Airport getAirport(Scanner user_input) {
	//
	//		return ConnectionMySQL.selectAirport(Queries.SHOW_AIRPORTS);
	//	}

	/**
	 * Skapar en flyglinje
	 * @param user_input
	 */
	public void createNewFlightLine(Scanner user_input) {
		String name, arrAirport, depAirport, arr_date;
		int company_id;

		boolean isUnique = false;

		System.out.println("\nVilket flygbolag ska äga flyglinjen: ");

		ConnectionMySQL.getCompany(Queries.SHOW_COMPANY);

		Company company = null;
		do {
			System.out.print("\nBolag, ange id: ");
			company_id = user_input.nextInt();
			isUnique = ConnectionMySQL
					.checkIfUnique(Queries.getCompanyWhereCompany_id(company_id));

			if (isUnique) {
				System.out
				.println("Detta flygbolag finns inte, ange ett nytt id!");
			}

		} while (isUnique);
		company = ConnectionMySQL
				.getCompanyInstance(Queries.getCompanyWhereCompany_id(company_id));

		System.out
		.println("\nDessa flyglinjer finns i systemet för valt bolag ("
				+ company.getMyName() + "):\t");


		ConnectionMySQL.getFlightline(Queries.getFlightlineWhereCompany_id(company));
		System.out.println("\n\nDessa flygplatser finns i systemet:");
	
		ConnectionMySQL
		.getAirport(Queries.SHOW_AIRPORTS);

		System.out
		.println("\nSkapa en ny flyglinje, skriv in prefix (tre bokstäver):");
		System.out.print("Från vilken flygplats: ");

		do {

			arrAirport = user_input.next().toUpperCase();
			isUnique = ConnectionMySQL
					.checkIfUnique(Queries.getAirportWhereArrAirport(arrAirport));

			if (isUnique) {
				System.out
				.println("Denna flygplats finns inte, ange ett nytt prefix!");
			}

		} while (isUnique);

		System.out.print("Till vilken flygplats: ");

		do {
			depAirport = user_input.next().toUpperCase();
			isUnique = ConnectionMySQL
					.checkIfUnique(Queries.getAirportWhereDepAirport(depAirport));

			if (isUnique) {
				System.out
				.println("Denna flygplats finns inte, ange ett nytt prefix!");
			}

		} while (isUnique);

		System.out.print("Namnge flyglinjen: \n");
		name = user_input.next();

		do {
			System.out
			.print("Skriv in datum (ÅÅÅÅMMDD) för flyglinjens avgång: \n");
			arr_date = user_input.next();

			isUnique = ConnectionMySQL
					.checkIfUnique(Queries.checkUniqueArrDate(name, arr_date));

			if (!isUnique) {
				System.out
				.println("Denna flyglinje finns redan inlagt för detta datum, prova nytt datum!");
			}
		} while (!isUnique);

		
		ConnectionMySQL.updateOrInsert(Queries.insertFlightline(name, arrAirport, depAirport, arr_date, company_id));

		int svar;
		do {
			System.out.println("\nVill du skapa nytt datum för flyglinjen: \n");
			System.out.println("1. Ja");
			System.out.println("2. Skapa ny flyglinje");
			System.out.println("3. Avsluta, återgå till huvudmeny");

			svar = user_input.nextInt();

			if (svar == 1) {

				do {
					System.out
					.print("Skriv in nytt datum (ÅÅÅÅMMDD) för flyglinjens avgång: \n");
					arr_date = user_input.next();

					isUnique = ConnectionMySQL
							.checkIfUnique(Queries.checkUniqueArrDate(name, arr_date));

					if (!isUnique) {
						System.out
						.println("Denna flyglinje finns redan inlagt för detta datum, prova nytt datum!");
					}
				} while (!isUnique);

				ConnectionMySQL
				.updateOrInsert("INSERT INTO ticketsystem.flightline (name,arrAirport,depAirport,arr_date,company_id,create_date) VALUES ('"
						+ name
						+ "','"
						+ arrAirport
						+ "','"
						+ depAirport
						+ "','"
						+ arr_date
						+ "','"
						+ company_id + "', now());");
				System.out.println("Nu är flyglinjen lagrad!");
			}
			if (svar == 2) {
				createNewFlightLine(user_input);
			}
			if (svar == 3) {
				Menu.mainMenu();
			}
		} while (true);
	}

	/**
	 * Skapar ett nytt flygbolag.
	 * 
	 * @param user_input
	 *            : Scanner instance
	 */
	public void createNewCompany(Scanner user_input) {
		Company company = new Company();

		String myName, myPrefix;
		int myEmployees, myAirplanes;

		System.out.println("\nDessa flygbolag finns i systemet:");

		ConnectionMySQL.getCompany(Queries.SHOW_COMPANY);

		System.out.println("Skapa ett flygbolag, minst sex tecken utan mellanslag: ");
		System.out.println("Skriv in namn på flygbolag: ");

		int chars = 0;
		boolean isUnique = false;
		do {
			myName = user_input.next();
			isUnique = ConnectionMySQL.checkIfUnique(Queries
					.checkUniqueCompany(myName));
			chars = myName.length();

			if (!isUnique) {
				System.out
				.println("Ett flygbolag med detta namn finns redan, ange ett nytt.");
			}
			if (chars < 6) {
				System.out.println("Namnet på flygbolaget är för kort ("
						+ chars + "), ange ett längre:");
			}

		} while (!isUnique || (chars < 6));

		company.setMyName(myName);

		System.out
		.println("\nSkriv in flygbolagets prefix (två bokstäver A-Z): \t");
		myPrefix = user_input.next();
		company.setMyPrefix(myPrefix);
		System.out.println("\nAntal anställda: ");
		myEmployees = user_input.nextInt();
		company.setMyEmployees(myEmployees);
		System.out.println("\nAntal flygplan: ");
		myAirplanes = user_input.nextInt();
		company.setMyAirplanes(myAirplanes);

		ConnectionMySQL
		.updateOrInsert("INSERT INTO ticketsystem.company (prefix,name,employees,airplanes,create_date) VALUES ('"
				+ company.getMyPrefix()
				+ "','"
				+ company.getMyName()
				+ "','"
				+ company.getMyEmployees()
				+ "','"
				+ company.getMyAirplanes() + "', now());");

		System.out.println("\nFlygbolaget (" + company.getMyName()
				+ ") finns nu inlagt i systemet!\n");
		ConnectionMySQL.getCompany(Queries.SHOW_COMPANY);
	}

	/**
	 * Söka och boka biljett
	 * @param user_input
	 */
	public void SearchBookingTicket(Scanner user_input) {

		Ticket ticket = new Ticket();
		Passenger passenger = new Passenger();
		Company company = null;
		Flightline flightline = null;

		String firstname, lastname, email, phonenumber;
		String arrAirport = null, depAirport = null, arr_date;
		boolean isNotUnique = false;

		ConnectionMySQL
		.getFlightline(Queries.SHOW_ALL_FLIGHTLINES);

		System.out.println("Ange avreseort (Från), ange prefix (ex ARN): ");
		do {
			arrAirport = user_input.next().toUpperCase();
			isNotUnique = ConnectionMySQL.checkIfUnique("SELECT * FROM ticketsystem.flightline WHERE arrAirport='"
							+ arrAirport + "';");

			if (!validateAirportPrefix(arrAirport)) {
				System.out
				.println("\nSkriv in prefix korrekt, ange tre bokstäver (A-Z): ");
			}

			if (isNotUnique) {
				System.out
				.println("En flygplats med detta prefix finns inte, ange ett nytt: ");
			}

		} while (((isNotUnique) || !validateAirportPrefix(arrAirport)));
//		flightline.setMyArrAirport(arrAirport);

		System.out.println("\nDessa destinationer (Till) finns att flyga till "
				+ arrAirport);
		ConnectionMySQL.getFlightline(Queries.getFlightlineWhereArrAirport(arrAirport));
	
		System.out.println("\nAnge destinationsort, ange prefix (ex ARN): ");

		do {
			depAirport = user_input.next().toUpperCase();
			isNotUnique = ConnectionMySQL
					.checkIfUnique("SELECT * FROM ticketsystem.flightline WHERE arrAirport='"
							+ arrAirport
							+ "' AND depAirport='"
							+ depAirport
							+ "';");

			if (!validateAirportPrefix(depAirport)) {
				System.out
				.println("\nSkriv in prefix korrekt, ange tre bokstäver (A-Z): ");
			}

			if (isNotUnique) {
				System.out
				.println("En flygplats med detta namn finns inte, ange ett nytt: ");
			}

		} while (((isNotUnique) || !validateAirportPrefix(depAirport)));
//				 flightline.setMyDepAirport(depAirport);

		System.out.println("\nVälj datum (ÅÅÅÅMMDD): ");

		do {
			arr_date = user_input.next();
			isNotUnique = ConnectionMySQL
					.checkIfUnique("SELECT * FROM ticketsystem.flightline WHERE arrAirport='"
							+ arrAirport
							+ "' AND depAirport='"
							+ depAirport
							+ "'AND arr_date='" + arr_date + "';");

			if (isNotUnique) {
				System.out
				.println("Ingen resa för detta datum finns tillgängligt, ändra datum!");
			}
		} while (isNotUnique);
		// flightline.setArr_date(arr_date);

		System.out.println("\nVill du fortsätta:");
		System.out.println("1. Boka vald resa");
		System.out.println("2. Avsluta");

		int svar = user_input.nextInt();

		do {
			if (svar == 1) {
				System.out.println("Ditt förnamn: ");
				firstname = user_input.next();
				passenger.setMyFirstname(firstname);
				System.out.println("Ditt efternamn: ");
				lastname = user_input.next();
				passenger.setMyLastname(lastname);
				System.out.println("Din email: ");
				email = user_input.next();
				passenger.setMyEmail(email);
				System.out.println("Ditt mobilnummer: ");
				phonenumber = user_input.next();
				passenger.setMyPhonenumber(phonenumber);
				ticket.setMyBookingnumber(phonenumber);

				ConnectionMySQL
				.updateOrInsert("INSERT INTO ticketsystem.passenger (firstname,lastname,email,phoneNumber,create_date) VALUES ('"
						+ passenger.getMyFirstname()
						+ "','"
						+ passenger.getMyLastname()
						+ "','"
						+ passenger.getMyEmail()
						+ "','"
						+ passenger.getMyPhonenumber() + "', now());");

				passenger = ConnectionMySQL
						.getPassengerInstance("SELECT * FROM ticketsystem.passenger where firstName='"
								+ firstname
								+ "' AND lastName='"
								+ lastname
								+ "' AND email='"
								+ email
								+ "' AND phoneNumber='" + phonenumber + "';");
				//
				flightline = ConnectionMySQL
						.getFlightlineInstance("SELECT * FROM ticketsystem.flightline WHERE arrAirport='"
								+ arrAirport
								+ "' AND depAirport= '"
								+ depAirport
								+ "' AND arr_date = '"
								+ arr_date
								+ "';");


				ConnectionMySQL
				.updateOrInsert("INSERT INTO ticketsystem.ticket (passenger_id,flightline_id,bookingnumber,create_date) VALUES ('"
						+ passenger.getMyPassenger_id()
						+ "','"
						+ flightline.getMyFlightline_id()
						+ "','"
						+ phonenumber + "', now());");

				
				System.out.println("\nDin resa är bokad och ditt bokningsnummer är: "
						+ phonenumber);

				System.out
				.println("\n-------------------------------------------");
				System.out.println("\nNAMN:");
				System.out.println(passenger.getMyFirstname() + " "
						+ passenger.getMyLastname() + "\n");
				System.out
				.println("-------------------------------------------");
				System.out.println("\n\n\n");
				Formatter f = new Formatter();
				System.out.println(f.format("%-10s %-5s %-5s %-10s",
						"DATUM", "FRÅN", "TILL", "FLYGNR"));
				Formatter g = new Formatter();
				System.out.println(g.format("%-10s %-5s %-5s %-10s",
						flightline.getArr_date(), flightline.getMyArrAirport(),
						flightline.getMyDepAirport(), flightline.getMyName()));
				System.out
				.println("\n\n\n-------------------------------------------");

			}
			if (svar == 2) {
				Menu.mainMenu();

			}
		} while (false);

	}

	/**
	 * Visar alla bokade biljetter
	 * @param user_input
	 */
	public void PrintAllTicket(Scanner user_input) {
		Ticket ticket = null;

		String query = ("SELECT ticketsystem.ticket.ticket_id AS Id, ticketsystem.FLIGHTLINE.arr_date AS Datum,"
				+ "ticketsystem.FLIGHTLINE.arrAirport AS Från, ticketsystem.FLIGHTLINE.depAirport AS Till,"
				+ "ticketsystem.FLIGHTLINE.name AS Flygnummer, ticketsystem.COMPANY.name AS Flygbolag,"
				+ "ticketsystem.ticket.bookingNumber AS Bokningsnummer, ticketsystem.passenger.firstName AS Förnamn, "
				+ "ticketsystem.passenger.lastName AS Efternamn,ticketsystem.ticket.create_date AS `Skapat datum` FROM ticketsystem.FLIGHTLINE "
				+ "INNER JOIN ticketsystem.COMPANY ON ticketsystem.FLIGHTLINE.COMPANY_ID = ticketsystem.COMPANY.COMPANY_ID "
				+ "JOIN ticketsystem.ticket ON ticketsystem.FLIGHTLINE.flightline_id = ticketsystem.ticket.flightline_id "
				+ "JOIN ticketsystem.passenger ON ticketsystem.ticket.passenger_id = ticketsystem.passenger.passenger_id");


		ConnectionMySQL.getAllTickets(query);

	}
	public void PrintMyTicket(Scanner user_input) {
		Ticket ticket = new Ticket();
		String answer;
		System.out.print("Ange bokningsnummer: ");
		answer= user_input.next();

		String query = ("SELECT ticketsystem.ticket.ticket_id AS Id, ticketsystem.FLIGHTLINE.arr_date AS Datum,"
				+ "ticketsystem.FLIGHTLINE.arrAirport AS Från, ticketsystem.FLIGHTLINE.depAirport AS Till,"
				+ "ticketsystem.FLIGHTLINE.name AS Flygnummer, ticketsystem.COMPANY.name AS Flygbolag,"
				+ "ticketsystem.ticket.bookingNumber AS Bokningsnummer, ticketsystem.passenger.firstName AS Förnamn, "
				+ "ticketsystem.passenger.lastName AS Efternamn,ticketsystem.ticket.create_date AS `Skapat datum` FROM ticketsystem.FLIGHTLINE "
				+ "INNER JOIN ticketsystem.COMPANY ON ticketsystem.FLIGHTLINE.COMPANY_ID = ticketsystem.COMPANY.COMPANY_ID "
				+ "JOIN ticketsystem.ticket ON ticketsystem.FLIGHTLINE.flightline_id = ticketsystem.ticket.flightline_id "
				+ "JOIN ticketsystem.passenger ON ticketsystem.ticket.passenger_id = ticketsystem.passenger.passenger_id WHERE ticketsystem.ticket.bookingNumber='"+answer+"';");//WHERE ticket.bookingNumber='"+answer+"';");
		ConnectionMySQL.getAllTickets(query);
	}

}
