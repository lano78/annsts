package ann.se.ticketsystem;


public class Queries {
	// Queries.SHOW_FLIGHTLINES
	public static String SHOW_AIRPORTS = "SELECT * FROM ticketsystem.airport order by prefix;";
	public static String SHOW_COMPANY = "SELECT * FROM ticketsystem.company order by name;";
	public static String SHOW_ALL_FLIGHTLINES="SELECT ticketsystem.FLIGHTLINE.FLIGHTLINE_ID AS `Id`, ticketsystem.FLIGHTLINE.ARR_DATE AS `Datum`"
			+ ",ticketsystem.FLIGHTLINE.NAME AS `Flyglinje`, ticketsystem.FLIGHTLINE.ARRAIRPORT AS `Från`, "
			+ "ticketsystem.FLIGHTLINE.DEPAIRPORT AS `Till`, ticketsystem.COMPANY.NAME AS `Flygbolag`, ticketsystem.FLIGHTLINE.CREATE_DATE AS `Ändringsdatum`"
			+ "FROM ticketsystem.FLIGHTLINE INNER JOIN ticketsystem.COMPANY ON ticketsystem.FLIGHTLINE.COMPANY_ID = ticketsystem.COMPANY.COMPANY_ID ORDER BY `Från`, `Datum`;";	
	public static String SHOW_FLIGHTLINES="SELECT flightline.name,flightline.arrAirport,flightline.depAirport, company.name FROM "+ ConnectionMySQL.database +".flightline "
	   		+ "INNER JOIN "+ConnectionMySQL.database+ ".company ON flightline.company_id = company.company_id;";
//	public static String CHECKUNIQUE_COMPANY = "SELECT * FROM ticketsystem.company WHERE name='";
	public static String CHECKUNIQUE_COMPANY_ID = "SELECT * FROM ticketsystem.company WHERE company_id='";

	public static String SHOW_ALL_BOOKING = "SELECT ticketsystem.ticket.ticket_id AS Id, ticketsystem.FLIGHTLINE.arr_date AS Datum,"
			+ "ticketsystem.FLIGHTLINE.arrAirport AS Från, ticketsystem.FLIGHTLINE.depAirport AS Till,"
			+ "ticketsystem.FLIGHTLINE.name AS Flygnummer, ticketsystem.COMPANY.name AS Flygbolag,"
			+ "ticketsystem.ticket.bookingNumber AS Bokningsnummer, ticketsystem.passenger.firstName AS Förnamn, "
			+ "ticketsystem.passenger.Efternamn AS LastName,ticketsystem.ticket.create_date AS 'Skapat datum' FROM ticketsystem.FLIGHTLINE "
			+ "INNER JOIN ticketsystem.COMPANY ON ticketsystem.FLIGHTLINE.COMPANY_ID = ticketsystem.COMPANY.COMPANY_ID "
			+ "JOIN ticketsystem.ticket ON ticketsystem.FLIGHTLINE.flightline_id = ticketsystem.ticket.flightline_id "
			+ "JOIN ticketsystem.passenger ON ticketsystem.ticket.passenger_id = ticketsystem.passenger.passenger_id";

	public static String SHOW_PASSENGER_BOOKING = "SELECT ticketsystem.ticket.ticket_id AS Id, ticketsystem.FLIGHTLINE.arr_date AS ArrivalDate,"
			+ "ticketsystem.FLIGHTLINE.arrAirport AS ArrAirport, ticketsystem.FLIGHTLINE.depAirport AS DepAirport,"
			+ "ticketsystem.FLIGHTLINE.name AS FlightlineName, ticketsystem.COMPANY.name AS Company,"
			+ "ticketsystem.ticket.bookingNumber AS BookingNumber, ticketsystem.passenger.firstName AS FirstName, "
			+ "ticketsystem.passenger.lastName AS LastName,ticketsystem.ticket.create_date AS CreateDate FROM ticketsystem.FLIGHTLINE "
			+ "INNER JOIN ticketsystem.COMPANY ON ticketsystem.FLIGHTLINE.COMPANY_ID = ticketsystem.COMPANY.COMPANY_ID "
			+ "JOIN ticketsystem.ticket ON ticketsystem.FLIGHTLINE.flightline_id = ticketsystem.ticket.flightline_id "
			+ "JOIN ticketsystem.passenger ON ticketsystem.ticket.passenger_id = ticketsystem.passenger.passenger_id WHERE BookingNumber = '";
	public Queries() {
		// TODO Auto-generated constructor stub
	}

	public static String checkUniqueCompany(String name){
		  return "SELECT * FROM ticketsystem.company WHERE name='"  + name + "';";
	}

	public static String showPassengerBooking(String bookingnumber){
		  return SHOW_PASSENGER_BOOKING  + bookingnumber + "';";
	}
	
	public static String getAirportWherePrefix(String prefix){
		return "SELECT * FROM ticketsystem.airport WHERE prefix= '" + prefix + "';";
	}
	
	public static String getAirportWhereArrAirport(String arrAirport){
		return "SELECT * FROM ticketsystem.airport WHERE prefix= '" + arrAirport + "';";
	}
	
	public static String getAirportWhereDepAirport(String depAirport){
		return "SELECT * FROM ticketsystem.airport WHERE prefix= '" + depAirport + "';";
	}
	
	public static String insertAirport(Airport airport){
		String query= "INSERT INTO ticketsystem.airport (city,country,prefix,create_date) VALUES ('"
				+ airport.getMyCity()
				+ "','"
				+ airport.getMyCountry()
				+ "','"
				+ airport.getMyPrefix() + "', NOW())";
		return query;
	}
	
	public static String insertFlightline(String name, String arrAirport, String depAirport, String arr_date, int company_id){
		String query = "INSERT INTO ticketsystem.flightline (name,arrAirport,depAirport,arr_date,company_id,create_date) VALUES ('"
				+ name
				+ "','"
				+ arrAirport
				+ "','"
				+ depAirport
				+ "','" + arr_date + "','" + company_id + "', now());";
		return query;
	}
	
	public static String getCompanyWhereCompany_id(int company_id){
		return "SELECT * FROM ticketsystem.company WHERE company_id='"
				+ company_id + "';";
	}
	
	public static String getFlightlineWhereCompany_id(Company company){
		String query= "SELECT flightline.flightline_id AS Id,flightline.arr_date AS Datum,flightline.name AS Flyglinje, flightline.arrAirport AS Från,flightline.depAirport AS Till , company.name AS Flygbolag,flightline.create_date AS Ändringsdatum "
			+ "FROM ticketsystem.flightline "
			+ "INNER JOIN ticketsystem.company ON flightline.company_id = company.company_id "
			+ "where company.company_id=" + company.getCompany_id();
		return query;
	}
	public static String getFlightlineWhereArrAirport(String arrAirport){
		String query= "SELECT flightline.flightline_id AS Id,flightline.arr_date AS Datum,flightline.name AS Flyglinje, flightline.arrAirport AS Från,flightline.depAirport AS Till , company.name AS Flygbolag,flightline.create_date AS Ändringsdatum "
			+ "FROM ticketsystem.flightline "
			+ "INNER JOIN ticketsystem.company ON flightline.company_id = company.company_id "
			+ "where flightline.arrAirport= '" + arrAirport +  "';";
		return query;
	}
	
public static String checkUniqueArrDate(String name, String arr_date){
	String query = "SELECT * FROM ticketsystem.flightline WHERE name='"
			+ name + "' AND arr_date='" + arr_date + "';";
	return query;
}






	
}
